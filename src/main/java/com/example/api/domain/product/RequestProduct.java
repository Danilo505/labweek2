package com.example.api.domain.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record RequestProduct(
        Long id,

        @NotBlank
        String name,
        String description,
        @NotNull
        Double price,
        Integer quantity ) {
}

