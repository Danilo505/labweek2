package com.example.api.controllers;

import com.example.api.domain.product.Product;
import com.example.api.domain.product.ProductRepository;
import com.example.api.domain.product.RequestProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){

        var allProducts = repository.findAll();
        return ResponseEntity.ok(allProducts);
    }

    @PostMapping
    public ResponseEntity<Product> registerProduct(@RequestBody @Validated RequestProduct data) {
        var product = new Product(data);
        repository.save(product);
        System.out.println(data);
        return ResponseEntity.status(HttpStatus.CREATED).body(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable long id, @RequestBody @Validated RequestProduct data){
        Product product = repository.findById(id).orElseThrow();
        product.setName(data.name());
        product.setDescription(data.description());
        product.setPrice(data.price());
        product.setQuantity(data.quantity());
        repository.save(product);
        return ResponseEntity.ok(product);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable long id){
       repository.deleteById(id);
       return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/name")
    public ResponseEntity<Product> getByNameProduct(@RequestParam String name){
        Product product = repository.findByName(name).orElseThrow();

        return ResponseEntity.ok(product);
    }

    @GetMapping("/price")
    public ResponseEntity<Product> getByPriceProduct(@RequestParam double price){
        Product product = repository.findByPrice(price).orElseThrow();

        return ResponseEntity.ok(product);
    }
}
